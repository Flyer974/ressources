clear;
format long; %Donne une valeur d�cimal longue

%On d�clare la valeur du contenu de la matrice M et B
M = [1 0 -0.85 0; -0.425 1 0 0; -0.425 -0.85 1 -0.1275; 0 0 0 1]; 
B = [0.15; 0.15; 0.15; 0.15];

%On calcule la valeur de PageRank
ValeurPageRank = M^(-1) * B;

ValeurPageRank